import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ses_ssion_1_5/auth/presentation/pages/signUpPage.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

void main() async {
  await Supabase.initialize(
      url: "https://czdvkuhvrfoyxllivtkw.supabase.co",
      anonKey: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImN6ZHZrdWh2cmZveXhsbGl2dGt3Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDkzOTYyNDcsImV4cCI6MjAyNDk3MjI0N30.D5ai7MeJKbAJkny_f-QCOC0GRE2r9G2moWlH9vsQMOA");
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: SignUpPage(),
    );
  }

}
