import 'package:flutter/material.dart';

var theme = ThemeData(
  textTheme: const TextTheme(
    titleLarge: TextStyle(
      color: Color.fromARGB(255, 106, 129, 249),
      fontSize: 24,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w700
    ),
    titleMedium: TextStyle(
      color: Color.fromARGB(255, 129, 129, 129),
      fontSize: 14,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w500,
    ),
    titleSmall: TextStyle(
      color: Color.fromARGB(255, 255, 255, 255),
      fontSize: 16,
      fontFamily: "Roboto",
      fontWeight: FontWeight.w500
    )
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      foregroundColor: Color.fromARGB(255, 255, 255, 255),
      backgroundColor: Color.fromARGB(255, 106, 129, 249),
      disabledBackgroundColor: Color.fromARGB(255, 129, 129, 129),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 14),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  )
);